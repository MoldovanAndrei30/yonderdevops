import pandas as pd
import requests

def excelData(apiUrl):
    response = requests.get(apiUrl)
    data = response.json()
    return data

apiUrl = 'http://localhost:30000/drivers-licenses/list'

def createExcelFile(apiUrl, excelFilePath='output_excel_file.xlsx'):
    apiData = excelData(apiUrl)
    df = pd.DataFrame(apiData)
    df.to_excel(excelFilePath, index=False)
    print(f"Excel file '{excelFilePath}' has been generated successfully.")

