import pandas as pd
from authority import LicenseAuthority

def main():
    authority = LicenseAuthority()
    authority.fetchData(totalPoints=150)

    while True:
        try:
            authority.fetchData()

            print("\n1. List suspended licenses")
            print("2. Extract valid licenses issued until today")
            print("3. Find licenses by category and count")
            print("4. Exit")

            operationId_str = input("Enter operation ID: ")

            if not operationId_str.isdigit():
                raise ValueError("Invalid input. Please enter a valid integer.")

            operationId = int(operationId_str)

            if operationId == 1:
                suspendedLicenses = authority.listSuspendedLicenses()
                print("Suspended Licenses:")
                for license in suspendedLicenses:
                    print(f"{license.nume} {license.prenume}")

                df = pd.DataFrame(suspendedLicenses)
                df.to_excel('suspendedLicenses.xlsx', index=False)
                print("Excel file 'suspendedLicenses.xlsx' generated.")

            elif operationId == 2:
                validLicenses = authority.exctractValidLicenses()
                print("Valid Licenses Issued Until Today:")
                for license in validLicenses:
                    print(f"{license.nume} {license.prenume}")

                df = pd.DataFrame(validLicenses)
                df.to_excel('validLicenses.xlsx', index=False)
                print("Excel file 'validLicenses.xlsx' generated.")

            elif operationId == 3:
                category = input("Enter license category: ")
                categoryCount = authority.findLicenseByCategory(category)
                print(f"Number of licenses in category {category}: {categoryCount}")

                df = pd.DataFrame({'License Category': [category], 'License Count': [categoryCount]})
                df.to_excel('licensesByCategory.xlsx', index=False)
                print("Excel file 'licensesByCategory.xlsx' generated.")

            elif operationId == 4:
                print("Exiting")
                break

            else:
                print("Invalid operation ID. Please try again.")

        except ValueError as e:
            print(f"Error: {e}")


if __name__ == "__main__":
    main()
