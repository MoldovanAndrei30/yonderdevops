import requests
from datetime import datetime
from driver_license import DriverLicense

class LicenseAuthority:
    def __init__(self):
        self.licenses = []

    def fetchData(self, totalPoints=150, perRequests=30):
        try:
            self.licenses = []

            numRequests = (totalPoints + perRequests - 1) // perRequests

            for _ in range(numRequests):
                response = requests.get("http://localhost:30000/drivers-licenses/list")

                if response.status_code == 200:
                    data = response.json()
                    self.licenses.extend([DriverLicense(**license_data) for license_data in data])
                else:
                    print(f"Failed to fetch data from API. Status code: {response.status_code}")
                    break

                if len(self.licenses) >= totalPoints:
                    break

        except Exception as e:
            print(f"Error: {e}")

    def listSuspendedLicenses(self):
        suspendedLicenses = [license for license in self.licenses if license.suspendat]
        return suspendedLicenses

    def exctractValidLicenses(self):
        today = datetime.now().strftime("%Y-%m-%d")

        validLicenses = [license for license in self.licenses if license.data_de_expirare >= today]
        return validLicenses

    def findLicenseByCategory(self, category):
        categoryCount = sum(1 for license in self.licenses if license.categorie == category)
        return categoryCount
