class DriverLicense:
    def __init__(self, id, nume, prenume, categorie, dataDeEmitere, dataDeExpirare, suspendat):
        self.id = id
        self.nume = nume
        self.prenume = prenume
        self.categorie = categorie
        self.data_de_emitere = dataDeEmitere
        self.data_de_expirare = dataDeExpirare
        self.suspendat = suspendat
